//-------------------------------------------------------------------------------------------------------------
var express        =         require("express");
var bodyParser     =         require("body-parser");
var app            =         express();
var PythonShell    =         require('python-shell');
var keyWord        =         0;
//-------------------------------------------------------------------------------------------------------------
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

//-------------------------------------------------------------------------------------------------------------
app.get('/',function(req,res){
  res.sendfile("index.htm");
});
//-------------------------------------------------------------------------------------------------------------
app.post('/',function(req,res){

  keyWord=req.body.query;
  console.log(keyWord);

  var options = {
    scriptPath: '',
    args: keyWord,
  };
  //-------------------------------------------------------------------------------------------------------------
  PythonShell.run('allSentimentAnalysis.py', options, function (err, results) {
    if (err) throw err;
    app.get('/index.htm', function(reqa, resa) {
    resa.set('Content-Type', 'application/javascript');
    });
   console.log('results: ', results);
    res.status(200).send(results);
  });
});
//-------------------------------------------------------------------------------------------------------------
app.listen(3000);
